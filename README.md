# jira-git

A Git extension for retrieving data from JIRA and creating local 
branches for issues.

![demo gif](https://bytebucket.org/tpettersen/jira-git/raw/master/docs/jira-git.gif)
 
# Setup

## Prerequisites

- [Node.js](https://nodejs.org/en/download/)
- [JIRA](www.atlassian.com/software/jira)
- [Git](https://www.atlassian.com/git)

## Installation

```
# to install
$ npm install -g jira-git
  
# to configure
$ git jira --configure
 
# help!
$ git jira -h
```

Note: **jira-git** should not be used with repo clones that are accessed 
by multiple users (e.g. on network shares) as your JIRA server, 
username, and password are stored per repo in `.git/jira`. The `.git/jira` 
file is set to be readable only by the owner, but your password is only 
base64 encoded, not encrypted.

# Usage

## `git jira` 

Display information about the JIRA issue linked to the current branch.

## `git jira JRA-123`

Display information about JIRA issue **JRA-123**.
 
## `git jira --branch JRA-123`

Create a new branch, or checkout an existing branch, for JIRA issue 
**JRA-123**.  

## `git jira --jql assignee = currentUser() and resolution = unresolved`

Display a list of unresolved issues assigned to me.

You may wish to create an alias for this (and any other commonly used 
JQL queries):

```
$ git config alias.todo "jira --jql assignee = currentUser() and resolution = unresolved"
$ git todo 
```

## Configuration

Once authenticated, you can add or modify the following properties in `.git/.jira`:

## `baseUrl`

The base URL of your JIRA instance (e.g. https://jira.atlassian.com)

## `username`

Your JIRA username (this is *not* necessarily your email address)

## `password`

Your JIRA password

## `branchNameFormat`

The format string for branches created with `-b`/`--branch` (defaults to `feature/%KEY%-%SUMMARY%`)

## `branchNameMaxSummaryLength`

Truncate issue summaries to this length for use in branch names (defaults to 50)

## `maxSummaryDisplayLength`

Truncate issue summaries to this length for display in terminal (defaults to 72)