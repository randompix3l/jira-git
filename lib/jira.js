var Promise = require("bluebird");
var chalk = require("chalk");
var request = require("superagent");
var _ = require("lodash");
var hashcode = require("hashcode").hashCode;

var program = require("./program");
var getConfig = require("./config");
var git = require("./git");
var handleError = require("./handleError");

function displayIssue(issueKey) {

    if (!(issueKey && issueKey.trim().length > 0)) {
        return handleError("Please specify an issue key.");
    }

    return Promise.coroutine(function* () {

        var config = yield getConfig();
        
        var res = yield request
            .get(config.apiUrl + "issue/" + issueKey)
            .auth(config.username, config.password)
            .set('Accept', 'application/json');

        if (!res.ok) {
            return handleError(res);
        }

        var issue = res.body;

        if (program.verbose) {
            console.log(JSON.stringify(issue, null, "  "));
        } else {
            console.log(colourFor(issue.key)(issue.key) + " " + chalk.white(truncateSummaryForDisplay(issue.fields.summary, config)));
            console.log(chalk.yellow("Type:\t\t") + " " + chalk.green(issue.fields.issuetype.name));
            if (issue.fields.priority) {
                console.log(chalk.yellow("Priority:\t") + " " + chalk.green(issue.fields.priority.name));
            }
            console.log(chalk.yellow("Status:\t\t") + " " + statusCategoryColour(issue.fields.status.statusCategory.key)(issue.fields.status.name));
            var resolution = (issue.fields.resolution && chalk.green(issue.fields.resolution.name)) || chalk.red("Unresolved");
            console.log(chalk.yellow("Resolution:\t") + " " + resolution);
            if (issue.fields.reporter) {
                console.log(chalk.yellow("Reporter:\t") + " " + issue.fields.reporter.displayName);
            }
            if (issue.fields.assignee) {
                console.log(chalk.yellow("Assignee:\t") + " " + issue.fields.assignee.displayName);
            }
        }

        var branches = [];
        yield git([
            "branch", "--list", "--all",                    // git branch --list --all <pattern ...>
            "*[!A-Za-z0-9]" + issue.key + "[!0-9]*",        // e.g. feature/JRA-1330-field-level-permissions
            "*[!A-Za-z0-9]" + issue.key,                    // e.g. feature/JRA-1330
            issue.key + "[!0-9]*",                          // e.g. JRA-1330-field-level-permissions
            issue.key                                       // e.g. JRA-1330
        ], {
            onLine: function(line) {
                var branch = line.trim();
                if (branch.length > 0) {
                    branches.push(branch);
                }
            }
        });

        console.log(chalk.yellow("Branches:"));

        if (program.branch) {
            var currentBranch = _.find(branches, function(branch) {
                return branch.charAt(0) === "*";
            });
            if (currentBranch) {
                console.log(chalk.red("  Already on '" + currentBranch.substring(2) + "'"));
                process.exit(1);
            }
            if (branches.length > 0) {
                yield git(["checkout", branches[0]]);
                branches.unshift("* " + branches.shift());
            } else {
                var newBranch = generateBranchName(issue, config);
                yield git(["checkout", "-b", newBranch]);
                newBranch = "* " + newBranch;
                branches.unshift(newBranch);
            }
        }

        if (branches.length) {
            branches.forEach(function(branch) {
                if (branch.indexOf("remotes/") === 0) {
                    console.log("  " + chalk.gray(branch));
                } else {
                    console.log("  " + chalk.green(branch));
                }
            });
        } else {
            console.log(chalk.red("  No branch matching '" + issue.key + "'"));
        }

    })().catch(function(err) {
        return handleError(err);
    });
}

function getContextIssueKey() {
    return Promise.coroutine(function* () {
        var branch;
        yield git(["rev-parse", "--abbrev-ref", "HEAD"], {
            onLine: function (line) {
                branch = line.trim();
            }
        });

        if (branch) {
            var results = /\b[A-Z]{1,10}-?[A-Z]+-\d+\b/.exec(branch);
            if (results) {
                return results[0];
            } else {
                console.log(chalk.red("Please specify an issue key, or checkout a branch with an issue key in its name."));
                process.exit(1);
            }
        }
    })();
}

function truncateSummaryForDisplay(summary, config) {
    if (summary.length > config.maxSummaryDisplayLength) {
        summary = summary.substring(0, config.maxSummaryDisplayLength) + "...";
    }
    return summary;
}

function generateBranchName(issue, config) {
    var branchName = config.branchNameFormat;

    branchName = branchName.replace("%KEY%", issue.key);
    
    var summarySlug = issue.fields.summary.replace(/[^A-Z0-9_-]/ig, "-");
    if (summarySlug.length > config.branchNameMaxSummaryLength) {
        summarySlug = summarySlug.substring(0, config.branchNameMaxSummaryLength);
    }
    while (summarySlug.indexOf("--") > -1) {
        summarySlug = summarySlug.replace("--", "-");
    }
    if (summarySlug.charAt(summarySlug.length - 1) === "-") {
        summarySlug = summarySlug.substring(0, summarySlug.length - 1);
    }
    branchName = branchName.replace("%SUMMARY%", summarySlug.toLowerCase());

    return branchName;
}

function executeJql(jql) {
    if (!(jql && jql.trim().length > 0)) {
        console.error(chalk.red("Please specify some JQL to execute."));
        process.exit(1);
    }

    return Promise.coroutine(function* () {
        var config = yield getConfig();
        var jqlUri = config.apiUrl + "search?jql=" + encodeURIComponent(jql);
        program.debug && console.log("jql URI: " + jqlUri);
        var res = yield request
            .get(jqlUri)
            .auth(config.username, config.password)
            .set('Accept', 'application/json');

        if (!res.ok) {
            return handleError(res);
        }

        if (program.verbose) {
            console.log(JSON.stringify(res.body, null, "  "));
        } else {
            for (var i = 0; i < res.body.issues.length; i++) {
                var issue = res.body.issues[i];
                console.log(
                    colourFor(issue.key)(issue.key) + " " +
                    chalk.white(truncateSummaryForDisplay(issue.fields.summary, config)) +
                    statusCategoryColour(issue.fields.status.statusCategory.key)(" (" + issue.fields.status.name + ")")
                );
            }
        }
    })().catch(function(err) {
        return handleError(err);
    });
}

var colours = [
    chalk.bold.bgGreen.black,
    chalk.bold.bgYellow.black,
    chalk.bold.bgRed.white,
    chalk.bold.bgBlue.white,
    chalk.bold.bgMagenta.white,
    chalk.bold.bgCyan.black,
    chalk.bold.bgWhite.black,
    chalk.bold.bgBlue.yellow,
    chalk.bold.bgBlue.cyan,
    chalk.bold.bgRed.yellow,
    chalk.bold.bgRed.cyan,
    chalk.bold.bgCyan.blue,
    chalk.bold.bgCyan.red,
    chalk.bold.bgYellow.red,
    chalk.bold.bgYellow.blue
];

function colourFor(issueKey) {
    var projectKey = issueKey.substring(0, issueKey.indexOf("-"));
    var hash = hashcode().value(projectKey);
    var colourIndex = Math.abs(hash % colours.length);
    return colours[colourIndex];
}

function statusCategoryColour(statusCategoryKey) {
    switch (statusCategoryKey) {
        case "new":
            return chalk.bold.cyan;
        case "indeterminate":
            return chalk.bold.yellow;
        case "done":
            return chalk.bold.green;
        default:
            return chalk.bold.white;
    }
}

module.exports = {
    displayIssue: displayIssue,
    getContextIssueKey: getContextIssueKey,
    executeJql: executeJql
};