var program = require("commander");

program
    .version("0.0.1")
    .arguments("[issue-key...]")
    .option("-b, --branch", "Checkout or create a branch for the specified issue.")
    .option("-j, --jql", "Execute a JQL query.")
    .option("-v, --verbose", "Display all issue details as JSON.")
    .option("-c, --configure", "Reconfigure JIRA URL, username, and password.")
    .option("-d, --debug", "Output debugging information.")
    .parse(process.argv);

module.exports = program;